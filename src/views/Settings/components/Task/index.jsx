import React, { Component } from 'react';

// Externals
import PropTypes from 'prop-types';
import classNames from 'classnames';
// import axios from 'axios';
import tasks from 'data/tasks';
// import Modal from 'react-responsive-modal';
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import { Redirect } from 'react-router';

// Material helpers
import { withStyles } from '@material-ui/core';

// Material components
import { TextField, Typography, Button } from '@material-ui/core';
import Icon from '@material-ui/core/Icon';
import img from './img.jpg';
import success from './success.svg';
import congrat from './congrat.jpg';

// Shared components
import {
  Portlet,
  PortletHeader,
  PortletLabel,
  PortletContent,
  PortletFooter
} from 'components';

// Component styles
import styles from './styles';
import DataProvider from "../../../../data/DataProvider";

const styles2 = theme => ({
  root: {
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
  title: {
    padding: 0
  }
});

const DialogTitle = withStyles(styles2)(props => {
  const { children, classes, onClose } = props;
  return (
    <MuiDialogTitle
      className={classes.title}
      disableTypography
    >
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton
          aria-label="Close"
          className={classes.closeButton}
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles(theme => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

const DialogActions = withStyles(theme => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);

const tasksID = [
  tasks[0].tasks[0].id,
  tasks[0].tasks[0].id,
  tasks[0].tasks[1].id,
  tasks[0].tasks[1].id
];

const variantsID = [
  tasks[0].tasks[0].variants[0].id,
  tasks[0].tasks[0].variants[1].id,
  tasks[0].tasks[1].variants[0].id,
  tasks[0].tasks[1].variants[1].id
];



function flatten(arr) {
    return arr.reduce(function (flat, toFlatten) {
        return flat.concat(Array.isArray(toFlatten) ? flatten(toFlatten) : toFlatten);
    }, []);
}

class Task extends Component {
  //mocked
  state = {
    index: 1,
    countRight: 0,
    tasksAmount: 0,
    tasksData: [],
    isEmpty: true,
    isError: false, //error when input is empty
    isRight: false,
    modalWrongIsOpen: false,
    modalCorrectIsOpen: false,
    modalFinalIsOpen: false,
    redirect: false,
    inputValue: '',
    expected: [],
    result: {
      'success-variants': [],
      'failed-variants': []
    }
  };


    componentDidMount() {
        DataProvider.startTest().then(
            test => {
                console.dir(test);
                let _tasksData = flatten(
                    test.tasks.map(task => task.variants)
                );
                console.dir(_tasksData);
                let rightValues = _tasksData
                    .map(elem => elem.answer.values[0]);
                this.setState({
                    tasksData: _tasksData,
                    tasksAmount: _tasksData.length,
                    expected: rightValues
                });
            }
        );
    }

  handleAnswer = () => {
    if (this.state.isEmpty) {
      this.setState({ isError: true });
    } else {
      // this.setState({ expected: this.state.tasksData[this.state.index - 1].answer.values[0] });
      if (this.state.inputValue.toLowerCase() === this.state.expected[this.state.index - 1]) {
        let a = this.state.result['success-variants'].slice()
        a.push({
          'variant-id': variantsID[this.state.index - 1],
          'task-id': tasksID[this.state.index - 1]
        })
        this.setState(prevState => ({
          result: {
            ...prevState.result,
            'success-variants': a,
          },
          isRight: true,
          countRight: this.state.countRight + 1
        }))
        this.onOpenCorrectModal();
      } else {
        let a = this.state.result['failed-variants'].slice()
        a.push({
          'variant-id': variantsID[this.state.index - 1],
          'task-id': tasksID[this.state.index - 1]
        })
        this.setState(prevState => ({
          result: {
            ...prevState.result,
              'failed-variants': a
          },
          isRight: false
        }))
        this.onOpenWrongModal();
      }

      // this.setState({ index: this.state.index + 1 });
      // axios
      //   .post('http://178.128.90.191/api/v1/test/start')
    }
  }

  handleInputChange = (event) => {
    this.setState({ inputValue: event.target.value });
    if (event.target.value === '') {
      this.setState({ isEmpty: true });
    } else {
      this.setState({ isEmpty: false });
    }
  }

  onOpenCorrectModal = () => {
    this.setState({ modalCorrectIsOpen: true });
  };

  onCloseCorrectModal = () => {
      let {index, tasksAmount} = this.state;
    if (index < tasksAmount) {
      this.setState({
        modalCorrectIsOpen: false,
        inputValue: '',
        index: index + 1
      });
    } else {
      this.onOpenFinalModal();
    }
  };

  onOpenWrongModal = () => {
    this.setState({ modalWrongIsOpen: true });
  };

  onCloseWrongModal = () => {
      let {index, tasksAmount} = this.state;
      if (index < tasksAmount) {
      this.setState({
        modalWrongIsOpen: false,
        inputValue: '',
        index: this.state.index + 1
      });
    } else {
      this.onOpenFinalModal();
    }
  };

  onOpenFinalModal = () => {
    this.setState({ modalFinalIsOpen: true });
  };

  onCloseFinalModal = () => {
    this.setState({
       modalFinalIsOpen: false,
       redirect: true
    });
  };

  render() {
    if (this.state.redirect) {
      return <Redirect push to="/welcome" />;
    }
  
    const { classes, className, ...rest } = this.props;
    const { tasksData, index } = this.state;
    const rootClassName = classNames(classes.root, className);

    return (
      <Portlet
        {...rest}
        className={rootClassName}
      >
        <PortletHeader>
          <PortletLabel
            // subtitle={}
            title="Задание"
          />
        </PortletHeader>
        <PortletContent noPadding>
          <form className={classes.form}>
            <div className={classes.group}>
              <Typography
                className={classes.groupLabel}
                variant="h4"
              >
                {(() => {
                    if (tasksData && tasksData.length && index) {
                        return tasksData[index - 1].description
                            .split('\n').map((item, i) =>
                                <p
                                    className={i === 0 ? classes.taskText : ''}
                                    key={i}
                                >
                                    {item}
                                </p>);
                    }
                })()
                }
              </Typography>
              <TextField
                className={classes.textField}
                error={this.state.isError}
                fullWidth
                id="outlined-name"
                label="Ответ"
                margin="normal"
                onChange={this.handleInputChange}
                required
                variant="outlined"
                autoComplete='off'
                value={this.state.inputValue}
              />
            </div>
          </form>
        </PortletContent>
        <PortletFooter className={classes.portletFooter}>
          {/* <Button
            color="primary"
            variant="outlined"
          >
            Пропустить
          </Button> */}
          <Button
            color="primary"
            onClick={this.handleAnswer}
            variant="contained"
          >
            Готово
            <Icon className={classes.rightIcon}>send</Icon>
          </Button>
        </PortletFooter>
        {/* <Modal
          onClose={this.this.onCloseModal}
          open={this.state.modalIsOpen}
        >
          <h2>Simple centered modal</h2>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam
            pulvinar risus non risus hendrerit venenatis. Pellentesque sit amet
            hendrerit risus, sed porttitor quam.
          </p>
        </Modal> */}
        <Dialog
          aria-labelledby="customized-dialog-title"
          onClose={this.onCloseCorrectModal}
          open={this.state.modalCorrectIsOpen}
        >
          <DialogTitle
            className={classes.modalHeader}
            style={{backgroundImage: `url('img.png')`}}
            id="customized-dialog-title"
            onClose={this.onCloseCorrectModal}
          >
          </DialogTitle>
          <DialogContent component={'div'} dividers>
            
            <p>
              <h2 className={classes.successTitle}>Верно!</h2>
              <img alt="img" className={classes.successImage} src={success} width="50%"/>
            </p>
          </DialogContent>
          <DialogActions>
            <Button
              variant="contained"
              color="primary"
              onClick={this.onCloseCorrectModal}
            >
              ОК
            </Button>
          </DialogActions>
        </Dialog>
        <Dialog
          aria-labelledby="customized-dialog-title"
          onClose={this.onCloseWrongModal}
          open={this.state.modalWrongIsOpen}
        >
          <DialogTitle
            className={classes.modalHeader}
            id="customized-dialog-title"
            onClose={this.onCloseWrongModal}
          >
            <img alt="img" src={img} width="100%"/>
          </DialogTitle>
          <DialogContent component={'div'} dividers>
            <h2>Ты ответил неправильно!</h2>
            <br/>
            <h3>Верный ответ: "{this.state.expected[this.state.index - 1]}"</h3>
            <br/>
            <div>
              <p>{(() => {
                  let {tasksData, index} = this.state;
                  if (tasksData && tasksData.length && index) {
                      return tasksData[index - 1].answer.details;
                  }
              })()}</p>
              <br/>
              <div>
                Мы рекомендуем изучить материал по теме <a href={tasks[0].tasks[0].resources[0]}>"{tasks[0].tasks[0].name}"</a>
              </div>
            </div>
          </DialogContent>
          <DialogActions>
            <Button
              variant="contained"
              color="primary"
              onClick={this.onCloseWrongModal}
            >
              ОК
            </Button>
          </DialogActions>
        </Dialog>
        <Dialog
          aria-labelledby="customized-dialog-title"
          onClose={this.onCloseFinalModal}
          open={this.state.modalFinalIsOpen}
        >
          <DialogTitle
            className={classes.modalHeader}
            style={{backgroundImage: `url('img.png')`}}
            id="customized-dialog-title"
            onClose={this.onCloseFinalModal}
          >
          </DialogTitle>
          <DialogContent dividers>
            
            <div>
              <h2 className={classes.doneTitle}>Твой результат: {this.state.countRight} из {this.state.tasksAmount}</h2>
              <img alt="img" className={classes.successImage} src={congrat} width="80%"/>
            </div>
          </DialogContent>
          <DialogActions>
            <Button
              variant="contained"
              color="primary"
              onClick={this.onCloseFinalModal}
            >
              ОК
            </Button>
          </DialogActions>
        </Dialog>
      </Portlet>
    );
  }
}

Task.propTypes = {
  className: PropTypes.string,
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Task);
