export default theme => ({
  root: {},
  form: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  group: {
    flexGrow: 1,
    padding: theme.spacing(2)
  },
  groupLabel: {
    // paddingLeft: theme.spacing(1)
  },
  field: {
    marginBottom: theme.spacing(2),
    marginTop: theme.spacing(2),
    display: 'flex',
    alignItems: 'center'
  },
  textField: {
    maxWidth: '100%',
    // marginLeft: theme.spacing(1),
  },
  portletFooter: {
    paddingLeft: theme.spacing(3),
    paddingRight: theme.spacing(3),
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
    display: 'flex',
    // justifyContent: 'space-between',
    justifyContent: 'flex-end',
  },
  rightIcon: {
    paddingLeft: theme.spacing(1),
    paddingRight: theme.spacing(3)
  },
  taskText: {
    marginBottom: theme.spacing(3),
  },
  modalHeader: {
    padding: 0
  },
  successImage: {
    display: 'block',
    margin: '0 auto'
  },
  successTitle: {
    textAlign: 'center',
    margin: '10px 0 30px',
    color: 'green'
  },
  doneTitle: {
    textAlign: 'center',
    margin: '10px 0 30px',
    color: 'black'
  }
});
