import React, { Component } from 'react';

// Externals
import PropTypes from 'prop-types';

// Material helpers
import { withStyles } from '@material-ui/core';

// Material components
import { Grid } from '@material-ui/core';

// Shared layouts
import { Dashboard as DashboardLayout } from 'layouts';

// Custom components
import { Task } from './components';

import background from '../../assets/img/lib.png';

// Component styles
const styles = theme => ({
  root: {
    // padding: theme.spacing(4),
  },

  rightSide: {
    // backgroundImage: "url(" + background + ")",
    // height:'75vh'
    textAlign: 'right'
  },
  leftSide: {
    flexGrow: 5,
  }
});

class Main extends Component {
  render() {
    const { classes } = this.props;

    return (
      <DashboardLayout title="EasyExam">
        <div className={classes.root}>
          <Grid
            container
            spacing={4}
          >
            <Grid
              item
              md={12}
              xs={12}
            >
              <div style={{
                display: 'flex',
                // backgroundColor: 'rgba(5, 17, 71, 1)'
              }}>
                <div className={classes.leftSide}/>
                <div className={classes.rightSide}>
                <img alt={'Background'}
                   src={background} style={{maxWidth: '50%'}}/>
                </div>
              </div>
            </Grid>
          </Grid>
        </div>
      </DashboardLayout>
    );
  }
}

Main.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Main);
