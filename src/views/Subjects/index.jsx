import React, { Component } from 'react';

// Externals
import PropTypes from 'prop-types';

// Material helpers
import {List, ListItem, ListItemIcon, ListItemText, withStyles} from '@material-ui/core';

// Material components
import { Grid } from '@material-ui/core';

// Shared layouts
import { Dashboard as DashboardLayout } from 'layouts';

// Custom components
import { Task } from './components';

import background from '../../assets/img/lib.png';
import {NavLink} from "react-router-dom";
import {AccountBoxOutlined as AccountBoxIcon, DashboardOutlined as DashboardIcon} from "@material-ui/icons";

// Component styles
const styles = theme => ({
  root: {
    // padding: theme.spacing(4),
  },

  rightSide: {
    // backgroundImage: "url(" + background + ")",
    // height:'75vh'
    textAlign: 'right'
  },
  leftSide: {
    flexGrow: 5,
  }
});

class Subject extends Component {
  render() {
    const { classes } = this.props;

    return (
      <DashboardLayout title="Выберите предмет">
        <div className={classes.root}>
          <Grid
            container
            spacing={4}
          >
            <Grid
              item
              md={12}
              xs={12}
            >
              <div style={{
                display: 'flex',
                justifyContent: 'center',
                // backgroundColor: 'rgba(5, 17, 71, 1)'
              }}>
                <List
                    component="div"
                    disablePadding
                >
                  <ListItem
                      activeClassName={classes.activeListItem}
                      className={classes.listItem}
                      component={NavLink}
                      to="/test"
                  >
                    <ListItemIcon className={classes.listItemIcon}>
                      <DashboardIcon />
                    </ListItemIcon>
                    <ListItemText
                        classes={{ primary: classes.listItemText }}
                        primary="Русский язык"
                    />
                  </ListItem>
                  <ListItem
                      activeClassName={classes.activeListItem}
                      className={classes.listItem}
                      component={NavLink}
                      to="/test"
                  >
                    <ListItemIcon className={classes.listItemIcon}>
                      <DashboardIcon />
                    </ListItemIcon>
                    <ListItemText
                        classes={{ primary: classes.listItemText }}
                        primary="Физика"
                    />
                  </ListItem>
                </List>
              </div>
            </Grid>
          </Grid>
        </div>
      </DashboardLayout>
    );
  }
}

Subject.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Subject);
