import React, { Component } from 'react';

// Externals
import PropTypes from 'prop-types';
import classNames from 'classnames';
// import axios from 'axios';
import tasks from 'data/tasks';
import Modal from 'react-responsive-modal';
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

// Material helpers
import { withStyles } from '@material-ui/core';

// Material components
import { TextField, Typography, Button } from '@material-ui/core';
import Icon from '@material-ui/core/Icon';

// Shared components
import {
  Portlet,
  PortletHeader,
  PortletLabel,
  PortletContent,
  PortletFooter
} from 'components';

// Component styles
import styles from './styles';

const styles2 = theme => ({
  root: {
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
});

const DialogTitle = withStyles(styles2)(props => {
  const { children, classes, onClose } = props;
  return (
    <MuiDialogTitle
      className={classes.root}
      disableTypography
    >
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton
          aria-label="Close"
          className={classes.closeButton}
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles(theme => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

const DialogActions = withStyles(theme => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);

class Task extends Component {
  //mocked
  state = {
    index: 1,
    tasksData: [
      tasks[0].tasks[0].variants[0],
      tasks[0].tasks[0].variants[1],
      tasks[0].tasks[1].variants[0],
      tasks[0].tasks[1].variants[1]
    ],
    isEmpty: true,
    isError: false, //error when input is empty
    modalIsOpen: false
  };

  // fetchTasks = () => {
  //   return axios
  //     .post('https://178.128.90.191/api/v1/test/start')
  //     .then(response => {
  //       return response.data.items.map(elem => {return { value: elem.name, label: elem.name }})
  //     })
  //     .catch((error) => {
  //       // eslint-disable-next-line no-console
  //       console.log(error);
  //     });
  // }

  // handleSkip = (selectedOption) => {
  //   this.setState({ selectedOption });
  //   axios
  //     .post()
  // }

  handleAnswer = () => {
    if (this.state.isEmpty) {
      this.setState({ isError: true });
    } else {
      this.onOpenModal();
      // this.setState({ index: this.state.index + 1 });
      // axios
      //   .post('http://178.128.90.191/api/v1/test/start')
    }
  }

  handleInputChange = (event) => {
    if (event.target.value === '') {
      this.setState({ isEmpty: true });
    } else {
      this.setState({ isEmpty: false });
    }
  }

  onOpenModal = () => {
    this.setState({ modalIsOpen: true });
  };

  onCloseModal = () => {
    this.setState({ modalIsOpen: false });
  };

  render() {
    const { classes, className, ...rest } = this.props;
    const rootClassName = classNames(classes.root, className);

    return (
      <Portlet
        {...rest}
        className={rootClassName}
      >
        <PortletHeader>
          <PortletLabel
            // subtitle={}
            title="Задание"
          />
        </PortletHeader>
        <PortletContent noPadding>
          <form className={classes.form}>
            <div className={classes.group}>
              <Typography
                className={classes.groupLabel}
                variant="h4"
              >
                {
                  this.state.tasksData[this.state.index - 1].description
                    .split('\n').map ((item, i) => 
                      <p
                        className={i === 0 ? classes.taskText : ''}
                        key={i}
                      >
                        {item}
                      </p>)
                }
              </Typography>
              <TextField
                className={classes.textField}
                error={this.state.isError}
                fullWidth
                id="outlined-name"
                label="Ответ"
                margin="normal"
                onChange={this.handleInputChange}
                required
                variant="outlined"
              />
            </div>
          </form>
        </PortletContent>
        <PortletFooter className={classes.portletFooter}>
          {/* <Button
            color="primary"
            variant="outlined"
          >
            Пропустить
          </Button> */}
          <Button
            color="primary"
            onClick={this.handleAnswer}
            variant="contained"
          >
            Готово
            <Icon className={classes.rightIcon}>send</Icon>
          </Button>
        </PortletFooter>
        {/* <Modal
          onClose={this.this.onCloseModal}
          open={this.state.modalIsOpen}
        >
          <h2>Simple centered modal</h2>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam
            pulvinar risus non risus hendrerit venenatis. Pellentesque sit amet
            hendrerit risus, sed porttitor quam.
          </p>
        </Modal> */}
        <Dialog
          aria-labelledby="customized-dialog-title"
          onClose={this.onCloseModal}
          open={this.state.modalIsOpen}
        >
          <DialogTitle
            className={classes.modalHeader}
            id="customized-dialog-title"
            onClose={this.onCloseModal}
          >
            Modal title
          </DialogTitle>
          <DialogContent dividers>
            <Typography gutterBottom>
              Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac
              facilisis in, egestas eget quam. Morbi leo risus, porta ac consectetur ac, vestibulum
              at eros.
            </Typography>
            <Typography gutterBottom>
              Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis
              lacus vel augue laoreet rutrum faucibus dolor auctor.
            </Typography>
            <Typography gutterBottom>
              Aenean lacinia bibendum nulla sed consectetur. Praesent commodo cursus magna, vel
              scelerisque nisl consectetur et. Donec sed odio dui. Donec ullamcorper nulla non metus
              auctor fringilla.
            </Typography>
          </DialogContent>
          <DialogActions>
            <Button
              color="primary"
              onClick={this.onCloseModal}
            >
              ОК
            </Button>
          </DialogActions>
        </Dialog>
      </Portlet>
    );
  }
}

Task.propTypes = {
  className: PropTypes.string,
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Task);
