import tasks from './tasks';
const BACKEND_HOST = process.env.REACT_APP_SERVER_URL;

class RemoteDataProvider {
    static startTest() {
        return fetch(BACKEND_HOST + '/api/v1/test/start', {
            method: 'POST',
            credentials: 'same-origin'
        })
            .then(response => {
                console.dir(response);
                if (response.status === 403 || response.status === 401) {
                    window.location.href = '/login';
                }
                return response;
            })
            .then(data => data.json());
    }
}

class LocalDataProvider {
    static startTest() {
        return Promise.resolve().then(() => tasks[0]);
    }
}

export default process.env.REACT_APP_DATA_SOURCE === 'remote' ? RemoteDataProvider : LocalDataProvider;
