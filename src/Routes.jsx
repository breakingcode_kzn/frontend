import React, {Component} from 'react';
import {Switch, Route, Redirect} from 'react-router-dom';

// Views
import Subject from './views/Subjects';
import Settings from './views/Settings';
import Main from './views/Main';
import UnderDevelopment from './views/UnderDevelopment';
import NotFound from './views/NotFound';

export default class Routes extends Component {
    render() {
        return (
            <Switch>
                <Redirect
                    exact
                    from="/"
                    to="/welcome"
                />
                <Redirect
                    exact
                    from="/index.html"
                    to="/welcome"
                />
                <Route
                    component={Main}
                    exact
                    path="/welcome"
                />
                <Route
                    component={Subject}
                    exact
                    path="/subjects"
                />
                <Route
                    component={Settings}
                    exact
                    path="/test"
                />
                <Route
                    component={UnderDevelopment}
                    exact
                    path="/under-development"
                />
                <Route
                    component={NotFound}
                    exact
                    path="/not-found"
                />
                <Redirect to="/not-found"/>
            </Switch>
        );
    }
}
