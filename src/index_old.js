import './assets/index.css';

const SERVER_HOST = process.env.NODE_ENV === 'production' ?
    '%BC_SERVER_ADDRESS_PLACEHOLDER%':
    process.env.REACT_APP_SERVER_URL;

/**
 * Просто проверяем работоспособность сервера получая всех пользователей
 */
class App {

    render(htmlTagId) {
        let tag = document.getElementById(htmlTagId);
        this.button(tag, 'get-all-button', () => {
            fetch(SERVER_HOST + '/api/v1/user/all')
                .then(response => {
                    return response.json();
                })
                .then(users => {
                    let resultField = document.getElementById('result-output');
                    console.dir(users);
                    resultField.innerText = JSON.stringify(users);
                })
                .catch(e => console.error(e));
        });
        this.result(tag, 'result-output');
    }

    result(htmlTag, id) {
        let element = document.createElement('textarea');
        element.id = id;
        element.title = id;
        htmlTag.appendChild(element);
    }

    button(htmlTag, id, onClick) {
        let element = document.createElement('input');
        element.id = id;
        element.type = 'button';
        element.title = id;
        element.value = 'Get all users';
        element.onclick = onClick;
        htmlTag.appendChild(element);
    }
}

new App().render('root');