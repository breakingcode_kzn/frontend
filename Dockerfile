FROM node:8.16.0-alpine
USER root
WORKDIR /build-dir
ADD / ./
RUN npm i && \
    npm run build